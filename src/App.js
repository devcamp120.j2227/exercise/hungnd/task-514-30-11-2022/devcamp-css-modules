import avatar from './assets/images/avatar.jpg';

import appStyle from './App.module.css';

function App() {
  return (
    <div>
      <div className={appStyle.devcampContainer}>
        <div>
          <img src={avatar} alt="avatar User" className={appStyle.devcampAvatar}></img>
        </div>
        <div className={appStyle.devcampQuote}>
          <p>This is one of the best developer blogs on the planet! I read it daily to improve my skills.</p>
        </div>
        <div>
          <span className={appStyle.devcampName}>
            <b>
              Tammy Stevens
            </b>
          </span>
          <span className={appStyle.devcampContent}>&nbsp; * &nbsp;Front End Developer</span>
        </div>
      </div>
    </div>
  );
}

export default App;
